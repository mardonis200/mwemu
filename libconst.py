"""
Constants.

Author: Vlad Topan (vtopan/gmail)
"""
import socket
import struct

from libmsg import err
import unicorn as u


REG_KEY_MAP = {
    'HKCC': 'HKEY_CURRENT_CONFIG',
    'HKCR': 'HKEY_CLASSES_ROOT',
    'HKCU': 'HKEY_CURRENT_USER',
    'HKLM': 'HKEY_LOCAL_MACHINE',
    'HKU': 'HKEY_USERS',
    }


class _DefDict(dict):
    """
    Used internally by PfxConst to allow easier use of the automagical value-to-name mappings.
    """

    def __getitem__(self, key):
        try:
            return super().__getitem__(key)
        except KeyError:
            res = str(key)
            self[key] = res
            return res



class PfxConst:
    """
    Base class which creates prefix-based dicts mapping values to names (e.g. .AF['INET'] == 2).
    """

    def __init_subclass__(cls):
        for k, v in list(cls.__dict__.items()):
            if k[0] != '_' and '_' in k:
                pfx, name = k.split('_', 1)
                for attr, value in [(pfx, f'{name}({v})'), ('NAME_' + pfx, f'{pfx}_{name}')]:
                    if not hasattr(cls, attr):
                        setattr(cls, attr, _DefDict())
                    getattr(cls, attr)[v] = value


class Win(PfxConst):
    """
    Windows constants.
    """

    NO_ERROR = 0
    SEC_E_OK = 0
    ERROR_SUCCESS = 0
    ERROR_FILE_NOT_FOUND = 2
    ERROR_PATH_NOT_FOUND = 3
    ERROR_INVALID_PARAMETER = 87
    ERROR_INSUFFICIENT_BUFFER = 122
    ERROR_MOD_NOT_FOUND = 126
    ERROR_PROC_NOT_FOUND = 127
    ERROR_MORE_DATA = 234

    LCMAP_LOWERCASE = 0x00000100
    LCMAP_UPPERCASE = 0x00000200
    LCMAP_TITLECASE = 0x00000300
    LCMAP_HALFWIDTH = 0x00400000
    LCMAP_FULLWIDTH = 0x00800000

    CP_ACP = 0
    CP_OEMCP = 1
    CP_MACCP = 2
    CP_THREAD_ACP = 3
    CP_SYMBOL = 42
    CP_UTF7 = 65000
    CP_UTF8 = 65001

    C1_UPPER = 0x0001
    C1_LOWER = 0x0002
    C1_DIGIT = 0x0004
    C1_SPACE = 0x0008
    C1_PUNCT = 0x0010
    C1_CNTRL = 0x0020
    C1_BLANK = 0x0040
    C1_XDIGIT = 0x0080
    C1_ALPHA = 0x0100
    C1_DEFINED = 0x0200

    HKEY_CLASSES_ROOT = 0x80000000
    HKEY_CURRENT_USER = 0x80000001
    HKEY_LOCAL_MACHINE = 0x80000002
    HKEY_USERS = 0x80000003
    HKEY_PERFORMANCE_DATA = 0x80000004
    HKEY_CURRENT_CONFIG = 0x80000005
    HKEY_DYN_DATA = 0x80000006
    HKEY_CURRENT_USER_LOCAL_SETTINGS = 0x80000007
    HKEY_PERFORMANCE_TEXT = 0x80000050
    HKEY_PERFORMANCE_NLSTEXT = 0x80000060

    REG_NONE = 0
    REG_SZ = 1
    REG_EXPAND_SZ = 2
    REG_BINARY = 3
    REG_DWORD = 4
    REG_DWORD_LITTLE_ENDIAN = 4
    REG_DWORD_BIG_ENDIAN = 5
    REG_LINK = 6
    REG_MULTI_SZ = 7
    REG_RESOURCE_LIST = 8
    REG_FULL_RESOURCE_DESCRIPTOR = 9
    REG_RESOURCE_REQUIREMENTS_LIST = 10
    REG_QWORD = 11
    REG_QWORD_LITTLE_ENDIAN = 11



class Net(PfxConst):
    """
    Networking constants.
    """

    AF_UNSPEC = 0
    AF_INET = 2
    AF_IPX = 6
    AF_APPLETALK = 16
    AF_NETBIOS = 17
    AF_INET6 = 23

    SOCK_STREAM = 1
    SOCK_DGRAM = 2
    SOCK_RAW = 3
    SOCK_RDM = 4
    SOCK_SEQPACKET = 5

    IPPROTO_ICMP = 1
    IPPROTO_IGMP = 2
    IPPROTO_TCP = 6
    IPPROTO_UDP = 17
    IPPROTO_ICMPV6 = 58
    IPPROTO_RM = 113

    BTHPROTO_RFCOMM = 3

    SOCKET_ERROR = 0xFFFFFFFF

    WSA_INVALID_PARAMETER = 87
    WSAEINVAL = 10022
    WSAETIMEDOUT = 10060


    @classmethod
    def win_sockaddr(cls, data):
        af = struct.unpack('<H', data[:2])[0]
        res = {'af': af, 'host': None, 'port': None}
        if af == Net.AF_INET:
            res['port'] = struct.unpack('>H', data[2:4])[0]
            res['host'] = socket.inet_ntoa(data[4:8])
        elif af == Net.AF_INET6:
            res['port'], res['flowinfo'] = struct.unpack('>HL', data[2:8])
            res['host'] = socket.inet_ntop(af, data[8:24])
        else:
            err(f'Cannot parse sockaddr for AF {Net.AF[af]}!')
        return res



class CRT(PfxConst):
    """
    C & CRT constants.
    """

    O_TEXT = 0x4000
    O_BINARY = 0x8000

    IOREAD = 0x0001
    IOWRT = 0x0002
    IOFBF = 0x0000
    IOLBF = 0x0040
    IONBF = 0x0004
    IOMYBUF = 0x0008
    IOEOF = 0x0010
    IOERR = 0x0020
    IOSTRG = 0x0040
    IORW = 0x0080
    IOAPPEND = 0x0200
    IOYOURBUF = 0x0100
    IOSETVBUF = 0x0400
    IOFEOF = 0x0800
    IOFLRTN = 0x1000
    IOCTRLZ = 0x2000
    IOCOMMIT = 0x4000
    IOFREE = 0x10000



class MEM:
    """
    Generic memory-related constants.
    """

    MEM_R = u.UC_PROT_READ
    MEM_W = u.UC_PROT_WRITE
    MEM_X = u.UC_PROT_EXEC
    MEM_RW = u.UC_PROT_READ | u.UC_PROT_WRITE
    MEM_RWX = u.UC_PROT_ALL



class X86:
    """
    x86 architecture constants, structures & auxiliary functions.
    """

    MSR_EFER = 0xC0000080
    MSR_STAR = 0xC0000081
    MSR_LSTAR = 0xC0000082
    MSR_CSTAR = 0xC0000083
    MSR_SF_MASK = 0xC0000084
    MSR_FS_BASE = 0xC0000100
    MSR_GS_BASE = 0xC0000101
    MSR_KERNEL_GS_BASE = 0xC0000102
    MSR_TSC_AUX = 0xC0000103
    MSR_TSC_RATIO = 0xC0000104
    MSR_TSC = 0x10
    MSR_SYSENTER_CS = 0x174
    MSR_SYSENTER_ESP = 0x175
    MSR_SYSENTER_EIP = 0x176
    MSR_CPUID_0 = 0xC0010030
    MSR_CPUID_1 = 0xC0010031
    MSR_CPUID_2 = 0xC0010032
    MSR_CPUID_3 = 0xC0010033
    MSR_CPUID_4 = 0xC0010034
    MSR_CPUID_5 = 0xC0010035

    X64_CALL_REG = {
        0: 'RCX',
        1: 'RDX',
        2: 'R8',
        3: 'R9'
    }


    @classmethod
    def make_gdt_type(cls, is_code, C=1, R=1, A=0, E=0, W=1):
        """
        Creates a raw Type field for a GDT descriptor.

        Code descriptors: C = conforming, R = readable, A = accessed
        Data descriptors: E = expand down, W = writable, A = accessed
        """
        if is_code:
            return 8 | (bool(C) << 2) | (bool(R) << 1) | bool(A)
        else:
            # todo: check expand down segments
            return (bool(E) << 2) | (bool(W) << 1) | bool(A)


    @classmethod
    def make_gdt_desc(cls, BaseAddr, SegLimit, Type, S=1, DPL=3, P=1, AVL=0, DB=1, G=1):
        """
        Creates a raw x86 GDT descriptor.
        """
        if BaseAddr > 0xFFFFFFFF or SegLimit > 0xFFFFF:
            raise ValueError(f'Invalid BaseAddr/SegLimit = 0x{BaseAddr:x}/0x{SegLimit:x}!')
        w0 = SegLimit & 0xFFFF
        w1 = BaseAddr & 0xFFFF
        w2 = ((BaseAddr >> 16) & 0xFF) | Type << 8 | S << 12 | DPL << 13 | P << 15
        w3 = ((SegLimit >> 16) & 0xFF) | AVL << 4 | DB << 6 | G << 7 | ((BaseAddr >> 24) << 8)
        return struct.pack('<Q', w0 | w1 << 16 | w2 << 32 | w3 << 48)


    @classmethod
    def make_seg_sel(cls, SI, TI=0, RPL=3):
        """
        Creates an x86 segment selector.
        """
        return SI << 3 | TI << 2 | RPL


