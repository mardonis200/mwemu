"""
Memory manager.

Author: Vlad Topan (vtopan/gmail)
"""
import array

from libbinview import MemView
from libemucpu import u
from libmisc import format_size
from libconst import MEM
from libmsg import log, err, dbg


HEAP_SIZE = 128 * 1024 * 1024
HEAP_CHUNK_SIZES = (64, 256, 1024, 4096, 16384, 65536, 262144, 1048576)
HEAP_PER_CHUNK = HEAP_SIZE // len(HEAP_CHUNK_SIZES)
DEFAULT_HEAP = -1



class MemMgr(MemView):
    """
    Memory manager.
    """

    def __init__(self):
        self.mmap = {}
        self.symbols, self.locations = {}, {}
        self.emulated = {}
        self.funinfo_cache = {}
        self.heaps = {None:None}
        self.tls_slots = []
        self.fls_slots = []
        self.size = self.max_addr = 0xFFFFFFFF
        super().__init__(mem=self, base=0)


    def init(self):
        if self.cpu.bits == 32:
            self.max_addr = 0xFFFFFFFF
        else:
            self.max_addr = 0xFFFFFFFFFFFFFFFF


    def init_mem(self, name, size, addr=None, prot=MEM.MEM_RWX):
        """
        Initialize a memory area.

        :param name: Area name.
        :param size: Size (must be a multiple of 4K).
        :param addr: If not given, one will be selected automatically.
        :param prot: A mix of MEM.{MEM_R,MEM_W,MEM_X}.
        """
        if size & 0xFFF:
            size += 0x1000 - (size & 0xFFF)
        if addr is None:
            addr = self.find_mem_gap(size)
        dbg(f'New memory block: {name} @ 0x{addr:x}[0x{size:x}]', 5)
        self.cpu.emu.mem_map(addr, size, perms=prot)
        if addr in self.mmap:
            raise ValueError(f'Block already mapped @ {addr:x}!')
        self.mmap[addr] = (size, name)      # todo: bisect to check for overlaps
        return addr


    def set_mem(self, addr, data=b'', size=None, name=None):
        """
        Set a memory range.

        :param name: If given, init the memory.
        """
        if type(data) is not bytes:
            data = bytes(data)
        if size:
            if len(data) < size:
                data += b'\0' * (size - len(data))
        elif data is not None:
            size = len(data)
        else:
            raise ValueError('Size or data must be supplied!')
        if name:
            self.init_mem(name, size, addr)
        if self.ldr.init_done:
            dbg(f'Mem write: {size} bytes @ 0x{addr:x}', 60)
        self.cpu.emu.mem_write(addr, data)


    def set_mem_prot(self, addr, size, prot=MEM.MEM_RWX):
        """
        Set memory protection flags.

        :param prot: a mix of MEM.{MEM_R,MEM_W,MEM_X}.
        """
        self.cpu.emu.mem_protect(addr, size, perms=prot)


    def get_mem(self, addr, size):
        """
        Read memory.
        """
        return self.cpu.emu.mem_read(addr, size)


    def get_mem_view(self, addr, size=None):
        """
        Returns a memory view (sliceable) object.
        """
        return MemView(self, addr, size)


    def find_mem_gap(self, size, start=0x2000000, roundto=0x10000):
        """
        Finds a memory gap of the given size.
        """
        res = None
        prev = 0
        for addr, (gsize, name) in sorted(self.mmap.items()):
            if addr >= start:
                if addr - max(prev, start) > size:
                    res = max(prev, start)
                    break
            prev = addr + gsize
            if prev % roundto:
                prev = (prev + roundto - 1) // roundto * roundto
        if res is None and prev + size < self.max_addr:
            res = prev
        if res is not None:
            dbg(f'Found 0x{size:X} bytes ({format_size(size)}) gap @ 0x{res:x}', 10)
            return res
        raise ValueError(f'Failed finding a free block of {size} bytes!')


    def add_symbol(self, name, addr):
        """
        Add a symbol to the map of known symbols.
        """
        self.locations[addr] = name
        self.symbols[name] = addr


    def hook_mem_read(self, callback, cbkdata=None, begin=None, end=None):
        """
        Hook memory writes.
        """
        self.cpu.emu.hook_add(u.UC_HOOK_MEM_WRITE, callback, user_data=cbkdata, begin=begin, end=end)


    def read_stack(self, count=1, offset=0):
        """
        Get values from the stack.
        """
        size = count * self.cpu.word
        addr = self.cpu.get_sp()
        values = []
        if self.is_valid_addr(addr) and self.is_valid_addr(addr + offset + size) \
                and 0 <= count < 100:
            data = self.get_mem(addr + offset, size)
            values = array.array(self.cpu.scode, data)
            # dbg(f'STACK @ {addr:x}: {data} => {values}', 50)
            if len(values) != count:
                raise ValueError(f'Failed unpacking {count} entries from the stack: {data}!')
        else:
            err(f'Invalid SP: 0x{addr} / count: {count}!')
        return list(values)


    def read_pchar(self, addr, decode=False, max_len=None):
        """
        Read a NULL-terminated string from addr as bytes (strip terminator).
        """
        if addr == 0:
            return None
        res = []
        while 1:
            res.append(self.cpu.emu.mem_read(addr, 1))
            addr += 1
            if res[-1] == b'\0':
                res = res[:-1]
                break
            if max_len and max_len >= len(res):
                break
        res = b''.join(res)
        if decode:
            enc = 'utf8' if type(decode) is not str else decode
            res = res.decode(enc, errors='replace')
        return res


    def read_pwchar(self, addr, decode=False, max_len=None):
        """
        Read a NULL-terminated widechar string from addr as bytes (strip terminator).
        """
        if addr == 0:
            return None
        res = []
        while 1:
            res.append(self.cpu.emu.mem_read(addr, 2))
            addr += 2
            if res[-1] == b'\0\0':
                res = res[:-1]
                break
            if max_len and max_len >= len(res):
                break
        res = b''.join(res)
        if decode:
            res = res.decode('utf16', errors='replace')
        return res


    def get_heap(self, heap_id):
        """
        Get a heap by ID/handle/address.
        """
        if heap_id is None and not self.heaps[heap_id]:
            self.heaps[None] = self.create_heap(HEAP_SIZE, tag=f'Heap(default)')
        return self.heaps.get(heap_id, None)


    def create_heap(self, size, tag=None):
        """
        Create a heap.
        """
        if not tag:
            tag = f'AnonHeap[{format_size(size)}]'
        heap = {'offs':0, 'bitmaps':{}, 'by_addr':{}, 'tag':tag, 'size':size}
        size = HEAP_SIZE    # fixme: handle actual heap size properly
        heap['addr'] = self.init_mem(tag, size)
        for sz in HEAP_CHUNK_SIZES:
            heap['bitmaps'][sz] = [0] * (size // sz)
        self.heaps[heap['addr']] = heap
        return heap


    def alloc_mem(self, size, tag=None, heap_id=None, zero_memory=False):
        """
        Allocate memory in the virtual heap.
        """
        heap = self.get_heap(heap_id)
        if not heap:
            return 0
        for i, (sz, bitmap) in enumerate(heap['bitmaps'].items()):
            if sz >= size:
                try:
                    idx = bitmap.index(0)
                except ValueError:
                    # no free slots in this chunk
                    err(f'WARNING: Heap chunks of size {sz} are full!')
                    continue
                bitmap[idx] = 1
                addr = heap['addr'] + i * HEAP_PER_CHUNK + idx * sz
                heap['by_addr'][addr] = (size, tag, sz, idx)
                if zero_memory:
                    self.write(addr, b'\0' * size)
                dbg(f'Allocated {size} bytes ({format_size(size)}) @ 0x{addr:x} (chunk:{sz}) on '
                        f'heap 0x{heap["addr"]:X}')
                return addr
        err(f'Failed allocating {size} bytes - heap 0x{heap["id"]:X} full!')
        return 0


    def free_mem(self, addr, heap_id=None):
        """
        Free allocated memory.
        """
        heap = self.get_heap(heap_id)
        if not heap or addr not in heap['by_addr']:
            err(f'Cannot free non-allocated block @ 0x{addr:x}!')
            return 0
        # todo: add hook to allow dumping / preserving freed data
        size, tag, chunk_sz, idx = heap['by_addr'][addr]
        heap['bitmaps'][chunk_sz][idx] = 0
        del heap['by_addr'][addr]
        return 1


    def get_heap_allocations(self, heap_id=None):
        """
        Iterates over heap allocations.

        :return: (addr, size, tag).
        """
        heap_id = heap_id or None
        heap = self.get_heap(heap_id)
        if heap:
            for addr, (size, tag, _, _) in heap['by_addr'].items():
                yield addr, size, tag


    def is_valid_addr(self, addr):
        """
        Checks if the given address is valid.
        """
        # todo: bisect
        if addr < 0 or addr > self.max_addr:
            return False
        for base, (size, name) in sorted(self.mmap.items()):
            if base > addr:
                break
            if addr >= base:
                if addr < base + size:
                    return True
        return False


    def tls_alloc(self):
        """
        Allocate TLS slot.
        """
        self.tls_slots.append(0)
        return len(self.tls_slots) - 1


    def fls_alloc(self, callback=None):
        """
        Allocate fiber local storage slot.
        """
        self.fls_slots.append({'callback': callback, 'value': 0})
        return len(self.fls_slots) - 1


    def fls_get_value(self, index):
        """
        Reads a fiber local storage value.
        """
        return self.fls_slots[index]['value']


    def fls_set_value(self, index, value):
        """
        Stores a fiber local storage value.
        """
        self.fls_slots[index]['value'] = value


    def dump_stack(self, count=6):
        """
        Dump the top `count` positions at the top of the stack.
        """
        count = min(count, (self.env.stack_top - self.cpu.get_sp()) // self.cpu.word)
        if count:
            stack = self.read_stack(count)
            stack = ' '.join('%0*X' % (self.cpu.word << 1, e) for e in stack)
        else:
            stack = 'empty'
        log('Stack: ' + stack)


    def dump_mem_ranges(self):
        """
        Dump memory ranges.
        """
        dbg('Memory regions:')
        for i, (begin, end, perms) in enumerate(sorted(self.cpu.emu.mem_regions())):
            sz = end - begin + 1
            sz = f'{sz >> 10:-3}KB' if sz < 1024 * 1024 else f'{sz >> 20:-3}MB'
            if self.cpu.bits == 32:
                dbg(f'  #{i:03}  {begin:08X}..{end:08X} ({sz})  {perms:04b}  {self.mmap[begin][1]}')
            else:
                dbg(f'  #{i:03}  {begin:016X}..{end:016X}  {perms}')


    def dump_symbols(self):
        """
        Dump known symbols.
        """
        log('Known symbols:')
        for k, v in sorted(self.symbols.items()):
            log(f'  {k} @ 0x{v:x}')


    def dump_heap_allocations(self):
        """
        Dump info about allocated chunks on the heap.
        """
        for heap_id, heap in self.heaps.items():
            if heap_id is None:
                continue
            if heap:
                log(f'Heap 0x{heap_id:X} @ 0x{heap["addr"]:X}:')
                for addr, size, tag in self.get_heap_allocations(heap_id):
                    log(f'  @0x{addr:08X}[0x{size:06X}] ({format_size(size):10})  {tag:20}')
                if not heap['by_addr']:
                    log(f'  Nothing allocated on heap 0x{heap_id:X}')



