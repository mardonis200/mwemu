"""
CPU emulator (based on Unicorn).

Author: Vlad Topan (vtopan/gmail)
"""
import unicorn as u
import unicorn.x86_const as cx86

from libconst import X86
from libdisasm import Disasm
from libmisc import format_flags
from libmsg import log, err, dbg


CPU_MODES = ('x86', 'x64',)
CARG_TYPE_REV_MAP = {
    'pchar': ('lpcstr', 'lpstr', 'char*'),
    'pwchar': ('pwstr', 'lpcwstr', 'lpwstr', 'wchar*'),
    'bool': ('boolean',),
    }
CARG_TYPE_MAP = {e:k for k, v in CARG_TYPE_REV_MAP.items() for e in v}



class CPUEmu:
    """
    CPU + Memory emulator.

    :param mode: Processor mode ('x86' or 'x64').
    :param trace_code: Exit to Python on each instruction (slow, but most features depend on this).
    """


    def __init__(self, mode='x86', trace_code=False, log_apis=True):
        self.arch = 'X86'
        if mode == 'x86':
            cpuarch, cpumode = u.UC_ARCH_X86, u.UC_MODE_32
            self.bits, self.word, self.scode = 32, 4, 'L'
            self.ip_reg = cx86.UC_X86_REG_EIP
            self.sp_reg = cx86.UC_X86_REG_ESP
        elif mode == 'x64':
            cpuarch, cpumode = u.UC_ARCH_X86, u.UC_MODE_64
            self.bits, self.word, self.scode = 64, 8, 'Q'
            self.ip_reg = cx86.UC_X86_REG_RIP
            self.sp_reg = cx86.UC_X86_REG_RSP
        else:
            raise ValueError(f'Mode {mode} not supported!')
        self.emu = u.Uc(cpuarch, cpumode)
        self.emu.reg_write(cx86.UC_X86_REG_EFLAGS, 0x0200)    # IF
        self.disasm = Disasm(self.arch, self.bits)
        self.trace_code = trace_code
        self.log_apis = log_apis
        self.hooks = {}
        self.code_hooks = {}
        self.breakpoints = {}
        self.halted = False
        self.max_instr = 0
        if self.trace_code:
            self.emu.hook_add(u.UC_HOOK_CODE, self._hook_code)
        self.emu.hook_add(u.UC_HOOK_MEM_FETCH_UNMAPPED | u.UC_HOOK_MEM_FETCH_PROT,
            self._hook_bad_insfetch)
        self.emu.hook_add(u.UC_HOOK_MEM_READ_UNMAPPED | u.UC_HOOK_MEM_WRITE_UNMAPPED,
            self._hook_mem_unmapped)
        self.emu.hook_add(u.UC_HOOK_MEM_READ_PROT | u.UC_HOOK_MEM_WRITE_PROT, self._hook_mem_prot)
        self.emu.hook_add(u.UC_HOOK_INTR, self._hook_intr)
        self.auto_emulated = set()


    def set_reg(self, name, value=0):
        """
        Set a CPU register.
        """
        fmtv = f'0x{value:x}' if type(value) is int else str(value)
        dbg(f'{name} := {fmtv}', 50)
        name = name.upper()
        self.emu.reg_write(getattr(cx86, f'UC_{self.arch}_REG_{name}'), value)


    def get_reg(self, name):
        """
        Read a CPU register.
        """
        name = name.upper()
        return self.emu.reg_read(getattr(cx86, f'UC_{self.arch}_REG_{name}'))


    def get_sp(self):
        """
        Get the stack pointer.
        """
        return self.emu.reg_read(self.sp_reg)


    def set_sp(self, addr):
        """
        Set the stack pointer.
        """
        self.emu.reg_write(self.sp_reg, addr)


    def push(self, value):
        """
        Push value on stack.
        """
        sp = self.get_sp()
        sp -= self.word
        self.mem.set_uint(sp, self.word, value)
        self.set_sp(sp)


    def get_ip(self):
        """
        Get the instruction pointer.
        """
        return self.emu.reg_read(self.ip_reg)


    def set_ip(self, addr):
        """
        Set the instruction pointer.
        """
        self.emu.reg_write(self.ip_reg, addr)


    def get_msr(self, msr):
        """
        Read an MSR.
        """
        return self.emu.reg_read(cx86.UC_X86_REG_MSR, msr)


    def set_msr(self, msr, value):
        """
        Set an MSR.
        """
        return self.emu.reg_write(cx86.UC_X86_REG_MSR, (msr, value))


    def set_max_instr(self, max_instr):
        """
        Set maximum number of instructions to run per run()/step() call (0 to disable).
        """
        self.max_instr = max_instr


    def step(self, ip=None, count=1):
        """
        Step one instruction from the current IP.
        """
        if count == 0:
            dbg(f'Starting continuous emulation from IP 0x{self.EIP:X}...')
        self.emu.emu_start(ip or self.EIP, 0, count=count)


    def run(self, ip=None):
        """
        Run from the given IP.
        """
        if 0 == self.max_instr:
            dbg(f'Emulating from IP 0x{self.get_ip():X}...')
        self.emu.emu_start(ip or self.get_ip(), 0, count=self.max_instr)


    def stop(self):
        """
        Stop emulation.
        """
        self.emu.emu_stop()
        self.halted = True


    def unhalt(self):
        """
        Allow emulation to continue (after a breakpoint or an error condition).
        """
        self.halted = False


    def _hook_mem_unmapped(self, uc, access, addr, size, value, user_data):
        err(f'Unmapped memory access ({access}) @ 0x{addr:X}[{size}] from IP 0x{self.EIP:X}!')
        self.stop()
        self.dump_state()
        return False


    def _hook_mem_prot(self, uc, access, addr, size, value, user_data):
        access = 'read' if access == 23 else 'write'
        err(f'Protected memory {access} @ 0x{addr:X}[{size}] (value=0x{value:x}) from IP 0x{self.EIP:X}!')
        self.stop()
        self.dump_state()
        return False


    def _hook_bad_insfetch(self, uc, access, addr, size, value, user_data):
        err(f'Unmapped/protected memory executed @ 0x{addr:X}[{size}] from IP 0x{self.EIP:X}!')
        self.stop()
        self.dump_state()
        return False


    def _hook_intr(self, uc, intno, user_data):
        err(f'Interrupt {intno} @ IP 0x{self.EIP:X}!')
        self.stop()
        self.dump_state()
        return False


    def _hook_code(self, uc, addr, size, user_data):
        # ip = self.get_ip()
        # insn = self.get_mem(addr, size)
        if addr in self.mem.locations:
            name = self.mem.locations[addr]
            log(f'SYM@IP: {name} @0x{addr:x}')
            if name == 'kernel32.dll:ExitProcess':
                self.stop()
                return


    def set_code_hook(self, callback, addr=None, user_data=None, size=1, api_args=None,
            format_args=False):
        """
        Install a code hook.

        :param callback: Function to call - prototype: (ip, retaddr, args, user_data).
        :param user_data: User data to be passed to the callback.
        :param addr: Address to hook (default: all).
        :param size: Number of bytes (from address) to hook (default: 1).
        :param api_args: Argument info - list of ("argtype", "argname").
        """
        if addr is not None:
            begin, end = addr, addr + size
        else:
            begin, end = 1, 0
        wrapped_callback = self.code_hook_wrapper(callback, api_args, format_args=format_args)
        hook_id = self.emu.hook_add(u.UC_HOOK_CODE, wrapped_callback, user_data=user_data,
                begin=begin, end=end)
        self.code_hooks[hook_id] = (callback, addr, size, api_args)
        return hook_id


    def del_code_hook(self, hook_id=None, addr=None):
        """
        Removecode hooks given by address or hook ID.
        """
        if hook_id:
            if hook_id not in self.code_hooks:
                raise ValueError(f'Invalid hook ID: {hook_id}!')
            dbg(f'Removing hook {hook_id} @ 0x{self.code_hooks[hook_id][2]:X}...')
            self.emu.hook_del(hook_id)
            del self.code_hooks[hook_id]
        elif addr:
            for hook_id, hook in list(self.code_hooks.items()):
                if hook[1] == addr:
                    dbg(f'Removing hook {hook_id} @ 0x{addr:X}...')
                    self.emu.hook_del(hook_id)
                    del self.code_hooks[hook_id]
        else:
            raise ValueError('Either hook_id or addr must be given!')


    def code_hook_wrapper(self, callback, api_args=None, format_args=False):
        """
        Generates a function to be used as a hook.
        """
        def _hook(uc, addr, size, user_data):
            retaddr, args = self.read_retaddr_and_args(api_args=api_args, format_args=format_args)
            return callback(addr, retaddr, args, user_data)
        return _hook


    def hook_api(self, lib, function, callback, callback_arg=None, format_args=False):
        """
        Hook library-based API call.
        """
        sym = f'{lib}:{function}'
        if sym not in self.mem.symbols:
            raise ValueError(f'Unknown symbol {sym}!')
        addr = self.mem.symbols[sym]
        funinfo = self.ldr.get_lib_info(lib)['exports'].get(function, None)
        api_args = None
        if funinfo and funinfo['args']:
            api_args = funinfo['args']
        self.set_code_hook(callback, addr=addr, user_data=callback_arg, api_args=api_args,
                format_args=format_args)


    def activate_step(self, ip):
        """
        Activate step (max_instr = 1) when this IP is hit.
        """
        def _hook(crtip, retaddr, args, user_data):
            log(f'Activating step (IP: 0x{crtip:X})...')
            self.max_instr = 1
            self.emu.emu_stop()
            self.del_code_hook(addr=ip)
        self.set_code_hook(_hook, addr=ip)


    def read_retaddr_and_args(self, api_args=0, format_args=False):
        """
        Assume the stack is just after a STDCALL API has been called and extract the return address
        and the arguments.
        """
        arg_cnt = len(api_args or [])
        stack = self.mem.read_stack(1 + arg_cnt)
        retaddr = stack[0]
        args = None
        if arg_cnt:
            args = stack[1:]
            if self.bits == 64:
                args = [self.emu.reg_read(reg) for reg in (cx86.UC_X86_REG_RCX, cx86.UC_X86_REG_RDX,
                        cx86.UC_X86_REG_R8, cx86.UC_X86_REG_R9)]
                if arg_cnt <= 4:
                    args = args[:arg_cnt]
                else:
                    args += stack[5:]
            if format_args:
                try:
                    args = [self.format_arg(api_args[i][0], api_args[i][1], v)
                            for i, v in enumerate(args)]
                except u.unicorn.UcError as e:
                    args = [f'0x{e:X}' for e in args]
                    err(f'Failed parsing API args: {e} (args: {", ".join(args)})!')
        return retaddr, args


    def api_log_hook(self, ip, retaddr, args, uarg):
        """
        Callback which logs all API.
        """
        lib, fun = uarg
        sym = f'{lib}:{fun}'
        comment = ' -- not emulated!' if sym not in self.mem.emulated and sym not in \
            self.auto_emulated else ''
        log(f'{ip:08X}:  API-CALL@{retaddr:08X} {sym}({", ".join(args or [])}){comment}')


    def api_ret(self, result, retaddr, argcnt):
        """
        Return from an emulated API call.
        """
        self.set_reg('EAX' if self.bits == 32 else 'RAX', result)
        self.set_ip(retaddr)
        pop = 1 + argcnt if self.bits == 32 else 1
        self.set_sp(self.get_sp() + pop * self.word)


    def set_breakpoint(self, addr):
        """
        Sets a breakpoint.
        """
        if addr not in self.breakpoints:
            self.breakpoints[addr] = self.set_code_hook(self.breakpoint_hook, addr=addr)
        else:
            err(f'Breakpoint already exists @ 0x{addr:x}!')


    def clear_breakpoint(self, addr):
        """
        Removes a breakpoint.
        """
        if addr in self.breakpoints:
            self.cpu.hook_del(self.breakpoints[addr])
            del self.breakpoints[addr]
        else:
            err(f'No breakpoints set @ 0x{addr:x}!')


    def breakpoint_hook(self, ip, retaddr, args, uarg):
        """
        Callback which logs all API.
        """
        log(f'BREAKPOINT:{ip:08X}')
        self.stop()


    def format_arg(self, atype, name, value):
        """
        Format a C value (API argument).
        """
        atype = atype.lower()
        atype = CARG_TYPE_MAP.get(atype, atype)
        if atype == 'pchar':
            res = self.mem.read_pchar(value)
            res = (f'0x{value:X}:' + repr(res)[1:]) if res is not None else 'NULL'
        elif atype == 'pwchar':
            res = self.mem.read_pwchar(value, decode=True)
            res = (f'0x{value:X}:' + repr(res)) if res is not None else 'NULL'
        elif atype == 'bool':
            res = bool(value)
        elif atype == 'word':
            res = f'0x{value:04X}'
        else:
            res = f'0x{value:08X}'
        return f'{name}={res}'



    def dump_regs(self):
        """
        Dump contents of registers.
        """
        regs = ('CS', 'DS', 'ES', 'FS', 'GS', 'SS', 'EFLAGS', 'CR0', 'CR4')
        if self.bits == 32:
            regs += ('EAX', 'EBX', 'ECX', 'EDX', 'ESP', 'EBP', 'ESI', 'EDI', 'EIP')
        else:
            regs += ('RAX', 'RBX', 'RCX', 'RDX', 'RSP', 'RBP', 'RSI', 'RDI', 'RIP', 'R8', 'R9',
                    'R10', 'R11', 'R12', 'R13', 'R14', 'R15') \
                    + tuple(f'R{i}' for i in range(9, 16))
        regs = {r:self.emu.reg_read(getattr(cx86, f'UC_{self.arch}_REG_{r}')) for r in regs}
        regs['flags'] = format_flags(regs['EFLAGS'])
        if self.bits == 32:
            log('CPU Registers:\n'
                '  EAX=%(EAX)08X EBX=%(EBX)08X ECX=%(ECX)08X EDX=%(EDX)08X  '
                'CR0=%(CR0)08X  CS=%(CS)04X DS=%(DS)04X ES=%(ES)04X  EFLAGS=%(EFLAGS)08X %(flags)s\n'
                '  ESP=%(ESP)08X EBP=%(EBP)08X ESI=%(ESI)08X EDI=%(EDI)08X  '
                'CR4=%(CR4)08X  FS=%(FS)04X GS=%(GS)04X SS=%(SS)04X  EIP=%(EIP)08X' % regs)
        else:
            log('CPU Registers:\n'
                '  RAX=%(RAX)016X RBX=%(RBX)016X RCX=%(RCX)016X RDX=%(RDX)016X  '
                'CR0=%(CR0)016X  CS=%(CS)04X DS=%(DS)04X ES=%(ES)04X  EFLAGS=%(EFLAGS)08X %(flags)s\n'
                '  RSP=%(RSP)016X RBP=%(RBP)016X RSI=%(RSI)016X RDI=%(RDI)016X  '
                'CR4=%(CR4)016X  FS=%(FS)04X GS=%(GS)04X SS=%(SS)04X  RIP=%(RIP)016X\n'
                '  R8 =%(R8)016X R9= %(R9)016X R10=%(R10)016X R11=%(R11)016X\n'
                '  R12=%(R12)016X R13=%(R13)016X R14=%(R14)016X R15=%(R15)016X' % regs)



    def dump_cpu_context(self, brief=False):
        """
        Dump disassembly from current IP.
        """
        ip = self.get_ip()
        if self.mem.is_valid_addr(ip):
            if brief:
                self.disasm.dump_disasm(None, self.mem[ip:ip + 32], address=ip, count=1, prefix='',
                        cpu=self)
            else:
                self.disasm.dump_disasm('Code @ IP:', self.mem[ip:ip + 32], address=ip, count=5,
                        cpu=self)
        else:
            err(f'Invalid IP: 0x{ip:x}!')


    def dump_state(self):
        """
        Dump as much info as possible about the current emulator state (e.g. on errors).
        """
        self.dump_regs()
        self.dump_cpu_context()
        self.mem.dump_stack()
        self.mem.dump_heap_allocations()


    def __getattr__(self, attr):
        """
        Allows direct access to registers.
        """
        reg = getattr(cx86, f'UC_{self.arch}_REG_{attr.upper()}', None)
        if reg is not None:
            return self.emu.reg_read(reg)
        raise AttributeError(f'{self.__class__} object has no attribute {attr}!')


