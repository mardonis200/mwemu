"""
Disassembler (based on Capstone).

Author: Vlad Topan (vtopan/gmail)
"""
import ast
import binascii
import re
import struct

import capstone as cs

from libmsg import log, err, dbg


TRANS = {
    'X86': cs.CS_ARCH_X86,
    32: cs.CS_MODE_32,
    64: cs.CS_MODE_64
    }

FMT_SIZE = {
    'byte': ('B', 1),
    'word': ('H', 2),
    'dword': ('L', 4),
    'qword': ('Q', 8),
    }

RX = {
    'mem-reg-offs': (r'([qd]?word|byte) ptr \[([er]?[abcdis0-9][px0-9]?)(?: ([+-]) (0?x?[a-f0-9]+))?\]', re.I),
    'reg': (r'(?:^|\W)([er][abcds0-9][ipx0-9])(?=\W|$)', re.I),
    'addr': (r'\[(0x[a-f0-9]{5,16})\]', re.I),
    'char-src': (r', 0x([2-7][0-9a-f])(?=\W|$)', re.I),
}
for k in RX:
    RX[k] = re.compile(*RX[k])



class Disasm:

    def __init__(self, arch, bits):
        self.arch = arch
        self.bits = bits
        self.eng = cs.Cs(TRANS[arch], TRANS[bits])


    def disasm(self, data, address):
        """
        Disassemble data.
        """
        res = self.eng.disasm(data, address)
        return res


    def dump_disasm(self, title, data, address, count=1, prefix='  ', cpu=None):
        """
        Dump disassembled data.

        :param cpu: If set (to a EmuCpu instance), the disassembly is annotated.
        """
        res = [title] if title else []
        for i, e in enumerate(self.disasm(data, address)):
            op_bytes = binascii.hexlify(e.bytes).decode('ascii').upper()
            instr = e.mnemonic + ' ' + e.op_str
            comment = ''
            ign_reg = set()
            if cpu:
                m = RX['addr'].findall(e.op_str)
                addrs = {int(x, 16):'dword' for x in m}
                m = RX['mem-reg-offs'].search(e.op_str)
                if m:
                    sz, reg, sign, offs = m.groups()
                    ign_reg.add(reg)
                    offs_val = ast.literal_eval(sign + offs) if offs else 0
                    addr = getattr(cpu, reg) + offs_val
                    addrs[addr] = sz
                m = sorted(set(x for x in RX['reg'].findall(e.op_str) if x not in ign_reg))
                if m:
                    reginfo = []
                    for reg in m:
                        val = getattr(cpu, reg)
                        reginfo.append(f'{reg}=0x{val:X}')
                        if val in cpu.mem.locations:
                            reginfo[-1] += '->' + cpu.mem.locations[val]
                    comment += '; ' + '; '.join(reginfo)
                for addr, sz in sorted(addrs.items()):
                    if addr in cpu.mem.locations:
                        comment += f'; 0x{addr:08X}=&{cpu.mem.locations[addr]}'
                    elif cpu.mem.is_valid_addr(addr):
                        fmt, size = FMT_SIZE[sz]
                        val = struct.unpack('<' + fmt, cpu.mem[addr:addr + size])[0]
                        if val in cpu.mem.locations:
                            val = f'{cpu.mem.locations[val]} (0x{val:X})'
                        else:
                            val = f"'{val:c}'" if 0x20 <= val < 0x7f else f'0x{val:X}'
                        comment += f'; {sz[0].upper()}[0x{addr:08X}]={val}'
                    else:
                        comment += f'; invalid addr: 0x{addr:X}'
                instr = RX['char-src'].sub(lambda m:f", '{chr(int(m[1], 16))}'", instr)
            res.append(f'{prefix}{e.address:08X}:  {op_bytes:32} {instr:40}  {comment}')
            if i == count - 1:
                break
        log('\n'.join(res))

