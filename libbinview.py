"""
Binary view classes.

Author: Vlad Topan (vtopan/gmail)
"""
import binascii
import struct


SIZE_MAP = {1:'B', 2:'H', 4:'L', 8:'Q'}


class BaseView:
    """
    View of binary data base class.
    """

    def set_w(self, addr, value):
        """
        Set WORD.
        """
        self[addr:addr + 2] = value


    def set_dw(self, addr, value):
        """
        Set DWORD.
        """
        self[addr:addr + 4] = value


    def get_dw(self, addr):
        """
        Get DWORD.
        """
        return struct.unpack('<L', self[addr:addr + 4])[0]


    def set_qw(self, addr, value):
        """
        Set QWORD.
        """
        self[addr:addr + 8] = value


    def get_qw(self, addr):
        """
        Get QWORD.
        """
        return struct.unpack('<Q', self[addr:addr + 8])[0]


    def get_uint(self, addr, size):
        """
        Get unsigned integer of given size.
        """
        return struct.unpack('<' + SIZE_MAP[size], self[addr:addr + size])[0]


    def set_uint(self, addr, size, value):
        """
        Set unsigned integer of given size.
        """
        self[addr:addr + size] = value


    def read(self, addr, size, encoding=None):
        """
        Read a buffer from mem.
        """
        buf = self[addr:addr + size]
        if encoding:
            buf = buf.decode(encoding, errors='replace')
        return buf


    def write(self, addr, buf, encoding=None):
        """
        Write a buffer to mem.
        """
        if encoding:
            buf = buf.encode(encoding, errors='replace')
        self[addr:addr + len(buf)] = buf
        return len(buf)


    def _slice_args(self, key):
        if type(key) is slice:
            start = key.start or 0
            size = (key.stop or self.size) - start
        else:
            start = key or 0
            size = 1
        return start, size


    def _pack(self, key, value):
        start, size = self._slice_args(key)
        if type(value) is int:
            value = struct.pack('<' + SIZE_MAP[size], value)
        return start, size, value


    def __str__(self):
        return self._repr()


    def __repr__(self):
        return self._repr()


    def _repr(self):
        return f'<Binary:{binascii.hexlify(self).decode("ascii").upper()}>'



class BinView(bytearray, BaseView):
    """
    bytearray view.
    """

    def __getitem__(self, key):
        res = super().__getitem__(key)
        return BinView(res)


    def __setitem__(self, key, value):
        start, size, value = self._pack(key, value)
        super().__setitem__(key, value)


    def __str__(self):
        return self._repr()


    def __repr__(self):
        return self._repr()



class MemView(BaseView):
    """
    Memory view - allows slice-based access to the emulator's memory.
    """

    def __init__(self, mem, base, size=None):
        self._mem = mem
        self._base = base
        self._size = size


    def __getitem__(self, key):
        start, size = self._slice_args(key)
        return BinView(self._mem.get_mem(start + self._base, size))


    def __setitem__(self, key, value):
        start, size, value = self._pack(key, value)
        self._mem.set_mem(start + self._base, data=value)



