"""
Generate API listings for DLLs.

Expects Windows DLLs to be in c:\windows\syswow64; pass an alternative path as the second parameter
if that's not the case. The DLL names to be processed are taken from dll-list.txt.

"""
import glob
import json
import os
import re
import sys

import pefile


if len(sys.argv) < 2:
    sys.exit(f'Usage: {sys.argv[0]} <path-to-winsdk-include> [<path-to-windows-dlls>]')
sdk_path = sys.argv[1]
dll_path = sys.argv[2] if len(sys.argv) > 2 else r'c:\windows\syswow64'
proj_path = os.path.dirname(os.path.dirname(__file__))
out_path = rf'{proj_path}\data\X86'
if not os.path.isdir(out_path):
    os.makedirs(out_path)
dlls = [x.strip() for x in open('dll-list.txt') if x.strip()]
exports = {e:{} for e in dlls}
# first pass: find all relevant functions from DLL exports
dllinfo = {}
for dll in dlls:
    print(f'Parsing {dll}...')
    pe = pefile.PE(os.path.join(dll_path, dll), fast_load=1)
    dllinfo[dll] = {'imagebase':pe.OPTIONAL_HEADER.ImageBase, 'exports':{}}
    pe.parse_data_directories(directories=[pefile.DIRECTORY_ENTRY['IMAGE_DIRECTORY_ENTRY_EXPORT']])
    for exp in pe.DIRECTORY_ENTRY_EXPORT.symbols:
        if exp.name:
            name = exp.name.decode('ascii')
            if '?' in name or '@' in name:
                continue
            exports[dll][name] = None
# second pass: extract function argument info from headers
rxs = {}
for dll in list(dlls):
    if not exports[dll]:
        del dlls[dll]
        sys.stderr.write(f'[!] No exports found in {dll}!\n')
        continue
    rxs[dll] = \
            re.compile(r'(?:#endif|(?:BOOL|WIN(?:OLE)?|WSA|NT|LWSTD)APIV?(?:_\(\w+\))?|'
                    r'(?:INTERNET|SHSTD)API_\(\w+\)|RPC_(?:VAR_)?ENTRY|APIENTRY)\s*('
            + '|'.join(exports[dll].keys()) + r')\s*\((.*?)\)\s*;', re.S)

for f in glob.glob(os.path.join(sdk_path, '*.h')):
    print(f'Looking for APIs in {f}...')
    data = open(f).read()
    for dll, rx in rxs.items():
        for m in rx.findall(data):
            members = m[1].replace(' *', '*').replace('*', '* ').replace(' OPTIONAL,', ',')
            if '\\' in members:
                continue    # macro
            cnt = 0
            while '(' in members:
                cnt += 1
                if cnt > 10:
                    sys.stderr.write(f'Failed extracting members from {m[1]}!')
                    break
                members = re.sub(r'\([^\(]+?\)', '', members).strip()
            if members.strip().lower() in ('', 'void'):
                # cnt = 0
                atypes = []
            else:
                # cnt = members.count(",") + 1
                args = [x.strip() for x in members.split(',')]
                try:
                    atypes = [[xx.strip() for xx in x.split(' ') if xx.strip()][-2:] for x in args]
                except IndexError:
                    sys.stderr.write(f'[!] Failed parsing arguments for {m[0]}:\n{members}\n')
            dllinfo[dll]['exports'][m[0]] = {'call': 'std', 'args': atypes}
for dll in dlls:
    dll = dll.lower()
    json.dump(dllinfo[dll], open(os.path.join(out_path, f'{dll}.json'), 'w'), sort_keys=1, indent=2)


