"""
Miscellaneous APIs.

Author: Vlad Topan (vtopan/gmail)
"""

import os
import struct

from libconst import Win


PROJ_PATH = os.path.dirname(__file__)
EFLAGS = ('CF', None, 'PF', None, 'AF', None, 'ZF', 'SF', 'TF', 'IF', 'DF', 'OF', None, None, 'NT',
        None, 'RF', 'VM', 'AC', 'VIF', 'VIP', 'ID')


def filetype(data):
    """
    Identify file type by header (at least 4K needed).
    """
    hdr = data[:4096]
    if data[:2] == b'MZ':
        elfanew = struct.unpack('<L', data[0x3C:0x40])[0]
        if elfanew + 8 < len(data):
            if data[elfanew:elfanew + 4] == b'PE\0\0':
                machine = data[elfanew + 4:elfanew + 6]
                if machine == b'\x64\x86':
                    return 'pe+'
                elif machine == b'\x4c\x01':
                    return 'pe'
                else:
                    return 'coff'
        return 'mz'
    elif data[:4] == b'\x7FELF':
        return 'elf'
    elif data[:4] == b'\xD0\xCF\x11\xE0':
        return 'docfile'
    elif data[:2] == b'PK':
        if b'META-INF/MANIFEST' in hdr and b'.class' in hdr:
            return 'jar'
        return 'zip'
    elif data[:4] == b'%PDF':
        return 'pdf'
    elif data[:5] == rb'{\rtf':
        return 'rtf'
    elif data[:5] == b'<html':
        return 'html'
    else:
        return 'unk'


def format_size(size):
    """
    Format a size in bytes.
    """
    for i, c in enumerate(('G', 'M', 'K', '')):
        v = (2 ** 10) ** (3 - i)
        if v <= size:
            s = str(int(size * 100 / v))
            s = s[:-2] if s.endswith('00') else s[:-2] + '.' + s[-2:]
            return s + f' {c}B'
    return '0B'


def format_flags(eflags):
    """
    Format EFLAGS as list of set flags.
    """
    res = []
    for i, flg in enumerate(EFLAGS):
        if eflags & (2 ** i):
            res.append(flg)
    return ' '.join(res)


def cp_to_encoding(cp):
    if cp == Win.CP_ACP:
        enc = 'mbcs'
    elif cp == Win.CP_OEMCP:
        enc = 'oemcp'
    elif cp in (20127,):
        enc = 'ascii'
    elif cp in (Win.CP_UTF7,):
        enc = 'U7'
    elif cp in (Win.CP_UTF8,):
        enc = 'utf8'
    elif cp in (1200,):
        enc = 'utf16'
    elif cp in (12000,):
        enc = 'utf32'
    elif cp in (12001,):
        enc = 'UTF-32BE'
    elif 1250 <= cp <= 1258 or cp in (874,):
        enc = f'windows-{cp}'
    elif 20273 <= cp <= 20424:
        enc = f'IBM{cp - 20000}'
    elif cp in (37, 437, 500, 737, 775, 850, 852, 855, 857, 858, 860, 861, 863, 864, 865, 869,
            870, 1026, 1047) or 1140 <= cp <= 1149:
        enc = f'IBM{cp}'
    elif 28591 <= cp <= 28605:
        enc = f'iso-8859-{cp-28590}'
    else:
        enc = None
    return enc

