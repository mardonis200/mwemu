#!/usr/bin/env python3
"""
Malware executable emulator.

Uses the Unicorn CPU emulation engine.

Author: Vlad Topan (vtopan/gmail)
"""
import os
import traceback

from libmisc import filetype, PROJ_PATH
from libemucpu import CPUEmu
from libemuenv import EnvEmu
from libmemmgr import MemMgr
from libldr import Loader
from libwinapi import install_winapi_hooks
from libmsg import log, err, dbg, CFG as MSG_CFG


COMPONENTS = ('cpu', 'mem', 'env', 'ldr')


class MwEmu:
    """
    Malware emulator.

    :param filename: File to emulate.
    :param mode: CPU mode (guessed from filename if not given).
    """

    def __init__(self, filename=None, mode=None, trace_code=False, log_apis=True,
            registry_source=None, debug=0):
        self.ft = None
        self.filename = filename
        MSG_CFG['debug_level'] = int(debug)
        if filename:
            self.open(filename)
        self.mode = mode
        self.trace_code = trace_code
        self.log_apis = log_apis
        self.proj_path = os.path.dirname(__file__)
        self.registry_source = registry_source
        if filename or mode:
            self.init()


    def init(self):
        """
        Initialize the emulator - called automatically if a filename or mode are given to __init__.
        """
        if not self.mode:
            if self.ft == 'pe':
                self.mode, self.envtype = 'x86', 'win'
            elif self.ft == 'pe+':
                self.mode, self.envtype = 'x64', 'win'
            else:
                raise ValueError(f'Unknown/invalid file type of {self.filename}!')
        self.cpu = CPUEmu(self.mode, trace_code=self.trace_code, log_apis=self.log_apis)
        self.mem = MemMgr()
        self.env = EnvEmu(registry_source=self.registry_source)
        self.ldr = Loader()
        # fixme: this needs more thinking
        for c1name in COMPONENTS:
            c1 = getattr(self, c1name)
            setattr(c1, 'mwemu', self)
            for c2name in COMPONENTS:
                if c1name == c2name:
                    continue
                c2 = getattr(self, c2name)
                setattr(c1, c2name, c2)
        self.mem.init()
        self.env.init()
        self.ldr.load(self.filename, main=True)
        if self.envtype == 'win':
            install_winapi_hooks(self)
        self.ldr.init_done = 1
        dbg(f'Emulator initialized. First entrypoint:0x{self.env.entrypoints[0][1]:X}')


    def open(self, filename):
        """
        Open (load) a file.
        """
        self.filename = filename
        hdr = open(filename, 'rb').read(4096)
        self.ft = filetype(hdr)



if __name__ == '__main__':
    import argparse
    import re
    import sys

    argp = argparse.ArgumentParser()
    argp.add_argument('target', help='malware file name')
    argp.add_argument('-s', '--step', help='step through code', action='store_true')
    argp.add_argument('-b', '--brief', help='brief emulated instruction log', action='store_true')
    argp.add_argument('-be', '--break-every', help='break after this many instructions', type=int,
            default=0)
    argp.add_argument('-D', '--debug', help='show debug messages', action='store_true')
    argp.add_argument('-as', '--activate-step', help='activate step by step at this IP',
            type=lambda x:int(x, 16), action='append')
    argp.add_argument('-dha', '--dump-heap-allocs', help='dump allocated data on heaps',
            action='store_true')
    argp.add_argument('-dh', '--dump-heaps', help='dump full heap contents', action='store_true')
    argp.add_argument('-dm', '--dump-end-snap', help='dump final mem snapshot', action='store_true')
    argp.add_argument('-dl', '--dump-lib', help='dump library')
    argp.add_argument('-ls', '--log-symbols', help='log known symbols', action='store_true')
    argp.add_argument('-lm', '--log-mem-ranges', help='log mem ranges', action='store_true')
    argp.add_argument('-rs', '--reg-source', help='registry source JSON', action='store_true',
            default=f'{PROJ_PATH}/data/win-registry.json')
    argp.add_argument('-o', '--logfile', help='logfile name')
    opts = argp.parse_args()

    cnt = 0
    if opts.logfile:
        sys.stderr = open(opts.logfile, 'w', encoding='utf8', errors='replace')

    log('Creating emulator instance...')
    filename = opts.target
    mwe = MwEmu(filename=filename, registry_source=opts.reg_source, debug=42 if opts.debug else 0)
    if opts.activate_step:
        for ip in opts.activate_step:
            mwe.cpu.activate_step(ip)
    mwe.cpu.set_max_instr(1 if opts.step else opts.break_every)
    if opts.log_mem_ranges:
        mwe.mem.dump_mem_ranges()
    if opts.log_symbols:
        mwe.mem.dump_symbols()

    log(f'Emulating {filename}...')
    while not mwe.cpu.halted:
        try:
            if not opts.brief:
                mwe.cpu.dump_regs()
                mwe.mem.dump_stack()
            mwe.cpu.dump_cpu_context(brief=opts.brief)
            mwe.cpu.run()
            cnt += 1
        except KeyboardInterrupt:
            log('QUIT!')
            break
        except Exception as e:
            err(f'Crashed: {e}')
            traceback.print_exc()
            break
    if opts.dump_heaps:
        log('Dumping heaps...')
        for heap in mwe.mem.heaps.values():
            addr, size = heap['addr'], heap['size']
            fn = f'heap-{addr:x}[0x{size:06X}]-full.dump'
            open(fn, 'wb').write(mwe.mem[addr:addr + size])
    if opts.dump_heap_allocs:
        log('Dumping heaps...')
        for heap_id in mwe.mem.heaps:
            if heap_id is None:
                continue
            for addr, size, tag in mwe.mem.get_heap_allocations(heap_id):
                tag = re.sub(r'[^\w@-]+', '_', tag)
                fn = f'heap-{heap_id:x}-0x{addr:08X}[0x{size:06X}]-{tag}.dump'
                open(fn, 'wb').write(mwe.mem[addr:addr + size])
    if opts.dump_end_snap:
        log('Dumping on-exit memory snapshot...')
        mwe.ldr.dump_image(f'{filename}.dump')
    if opts.dump_lib:
        lib = opts.dump_lib.lower()
        addr = mwe.ldr.img_by_name[lib].imagebase
        log(f'Dumping {lib} @ 0x{addr:X}...')
        mwe.ldr.dump_image(f'{lib}@{addr:X}.dump', addr=addr)
    log('Done.')


