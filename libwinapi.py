"""
Small Windows API implementation.

Author: Vlad Topan (vtopan/gmail)
"""
import inspect
import os
import socket
import struct
import time

from libmisc import format_size, cp_to_encoding
from libemuenv import H_INVALID
from libconst import Win, Net
from libmsg import log, err, dbg


MAX_CP_CHAR_SIZE = {
    2: [932, 936, 949, 950, 1361, 10001, 10002, 10003, 10008, 20000, 20001, 20002, 20003, 20004,
        20005, 20261, 20932, 20936, 20949, 51949, 55000, 55001, 55002, 55003, 55004],
    5: [50220, 50221, 50222, 50225, 50227, 50229, 52936, 65000],
    4: [54936, 57002, 57003, 57004, 57005, 57006, 57007, 57008, 57009, 57010, 57011, 65001]
    }
MAX_CP_CHAR_SIZE_REV = {e:k for k, v in MAX_CP_CHAR_SIZE.items() for e in v}

CP_DEF_CHAR = {
    0x6f: [37, 500, 870, 875, 1026, 1047, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148,
        1149, 20273, 20277, 20278, 20280, 20284, 20285, 20290, 20297, 20420, 20423, 20424, 20833,
        20838, 20871, 20880, 20905, 20924, 21025, 21027],
    0x9f81003f: [932, 10001],
    0xfe81003f: [936, 949, 950],
    0xd384003f: [1361],
    0xfc81003f: [10002],
    0xaca1003f: [10003, 20949, 51949],
    0xa9a1003f: [10008, 20936],
    0xfea1003f: [20000, 20004],
    0x8481003f: [20001, 20003],
    0xaf81003f: [20002],
    0xf58d003f: [20005],
    0xcfc1003f: [20261],
    0x8e8e003f: [20932],
    0x20: [55000, 55001, 55002, 55003, 55004]
    }
CP_DEF_CHAR_REV = {e:k for k, v in CP_DEF_CHAR.items() for e in v}

RECV_CNT = 0
RECV_MAX_CNT = 10   # stop after emulating this many socket recv()s as timeouts

STANDARD_EMULATION = ['kernel32.dll:InitializeCriticalSectionAndSpinCount',
        'kernel32.dll:EnterCriticalSection', 'kernel32.dll:LeaveCriticalSection',
        'kernel32.dll:SetHandleCount']


def install_winapi_hooks(mwemu):
    """
    Install the hooks defined in this library.
    """
    mwemu.cpu.auto_emulated |= set(STANDARD_EMULATION)
    for name, hookfun in globals().items():
        if name.startswith('api_'):
            _, lib, ext, fun = name.split('_', 3)
            lib = f'{lib}.{ext}'
            sym = f'{lib}:{fun}'
            hook = None
            if sym in mwemu.mem.symbols:
                hook = mwemu.cpu.hook_api(lib, fun, hookfun, mwemu)
            else:
                if sym not in mwemu.ldr.available_hooks:
                    mwemu.ldr.available_hooks[sym] = []
                mwemu.ldr.available_hooks[sym].append(hookfun)
                dbg(f'Delayed hook: {sym}', 5)
            mwemu.mem.emulated[sym] = (hookfun, hook)


def log_call(retaddr, args, res):
    api_name = inspect.getouterframes(inspect.currentframe(), 2)[1][3].rsplit('_')[-1]
    log(f'emulated:  API-CALL@{retaddr:08X} {api_name}({args})=>0x{res:08X}')


### modules & functions


def _loadlibrary(name_addr, encoding, emu):
    fun = emu.mem.read_pchar if encoding == 'ascii' else emu.mem.read_pwchar
    name = fun(name_addr, decode=True)
    if name_addr:
        modname = os.path.basename(name.lower())
        if not modname.endswith('.dll'):
            modname += '.dll'
        if modname in emu.ldr.img_by_name or emu.ldr.load_library(modname):
            res = emu.ldr.img_by_name[modname].imagebase
        else:
            res = 0
            emu.env.set_last_error(Win.ERROR_MOD_NOT_FOUND)
    else:
        res = emu.ldr.main_img.imagebase
    return res, name


def api_kernel32_dll_LoadLibraryA(ip, retaddr, args, emu):
    name_addr = args[0]
    res, name = _loadlibrary(name_addr, 'ascii', emu)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{repr(name)}', res)


def api_kernel32_dll_LoadLibraryW(ip, retaddr, args, emu):
    name_addr = args[0]
    res, name = _loadlibrary(name_addr, 'utf16', emu)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{repr(name)}', res)


def api_kernel32_dll_LoadLibraryExA(ip, retaddr, args, emu):
    name_addr, handle, flags = args
    res, name = _loadlibrary(name_addr, 'ascii', emu)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{repr(name)}, handle=0x{handle:X}, flags=0x{flags:X}', res)


def api_kernel32_dll_LoadLibraryExW(ip, retaddr, args, emu):
    name_addr, handle, flags = args
    res, name = _loadlibrary(name_addr, 'utf16', emu)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{repr(name)}, handle=0x{handle:X}, flags=0x{flags:X}', res)


def _getmodulehandle(modname, emu):
    if modname:
        if not modname.endswith('.dll'):
            modname += '.dll'
        if modname not in emu.ldr.img_by_name:
            res = 0
            err(f'GetModuleHandle*() called for unknown module {modname}!')
        else:
            res = emu.ldr.img_by_name[modname].imagebase
    else:
        res = emu.ldr.main_img.imagebase
    return res


def api_kernel32_dll_GetModuleHandleA(ip, retaddr, args, emu):
    name_addr = args[0]
    cpu = emu.cpu
    name = emu.mem.read_pchar(name_addr, decode=True)
    modname = None
    if name_addr:
        modname = os.path.basename(name.lower())
    res = _getmodulehandle(modname, emu)
    cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{repr(name)}', res)


def api_kernel32_dll_GetModuleHandleW(ip, retaddr, args, emu):
    name_addr = args[0]
    cpu = emu.cpu
    name = emu.mem.read_pwchar(name_addr, decode=True)
    modname = None
    if name_addr:
        modname = os.path.basename(name.lower())
    res = _getmodulehandle(modname, emu)
    cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{repr(name)}', res)


def _getmodulefilename(hmodule, name_addr, name_size, encoding, emu):
    res = 0
    filename = None
    if hmodule in emu.ldr.images:
        filename = emu.ldr.images[hmodule].filename
        res = len(filename)
    elif hmodule == 0:
        filename = emu.ldr.main_img.filename
        res = len(filename)
    if name_addr and filename:
        emu.mem.write(name_addr, filename[:name_size - 1] + '\0', encoding=encoding)
        if name_size < len(filename) + (1 if encoding == 'ascii' else 2):
            emu.env.set_last_error(Win.ERROR_INSUFFICIENT_BUFFER)
    return res


def api_kernel32_dll_GetModuleFileNameA(ip, retaddr, args, emu):
    hmodule, name_addr, name_size = args
    res = _getmodulefilename(hmodule, name_addr, name_size, 'ascii', emu)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'hmodule=0x{hmodule:X}, addr=0x{name_addr:X}, size={name_size}', res)


def api_kernel32_dll_GetModuleFileNameW(ip, retaddr, args, emu):
    hmodule, name_addr, name_size = args
    res = _getmodulefilename(hmodule, name_addr, name_size, 'utf16', emu)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'hmodule=0x{hmodule:X}, addr=0x{name_addr:X}, size={name_size}', res)


def api_kernel32_dll_GetProcAddress(ip, retaddr, args, emu):
    lib_ib, name_addr = args
    name = emu.mem.read_pchar(name_addr)
    emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
    res = 0
    if name_addr:
        fun = name.decode('ascii', errors='replace')
        if lib_ib not in emu.ldr.images:
            err(f'Unknown ImageBase 0x{lib_ib:x} in call to GetProcAddress({name})!')
        else:
            img = emu.ldr.images[lib_ib]
            res = emu.ldr.resolve_export(img.libname, fun) or 0
            emu.env.set_last_error(0 if res else Win.ERROR_PROC_NOT_FOUND)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'0x{lib_ib:08X}, {name}', res)


### Windows-specific API


def api_kernel32_dll_GetLastError(ip, retaddr, args, emu):
    res = emu.env.fs.get_dw(0x34)
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_SetLastError(ip, retaddr, args, emu):
    value = args[0]
    res = 0
    emu.env.fs.set_dw(0x34, value)
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'0x{value:08X}', res)


def api_user32_dll_GetMessageW(ip, retaddr, args, emu):
    res = 0     # simulate WM_QUIT
    emu.cpu.api_ret(res, retaddr, 4)
    log_call(retaddr, f'0x{args[0]:08X}, ...', res)


### timing


def api_kernel32_dll_GetSystemTimeAsFileTime(ip, retaddr, args, emu):
    stime = int(time.time())
    ptr = args[0]
    if ptr:
        # FILETIME structure
        emu.mem.set_dw(ptr, stime & 0xFFFFFFFF)
        emu.mem.set_dw(ptr + 4, stime >> 32)
    else:
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
    emu.cpu.api_ret(0, retaddr, 1)
    log_call(retaddr, f'', 0)


def api_kernel32_dll_GetTickCount(ip, retaddr, args, emu):
    res = int(time.time()) - emu.env.sys_start_time
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_QueryPerformanceCounter(ip, retaddr, args, emu):
    retval = int((time.time() - emu.env.sys_start_time) * 1000000)
    res = 1
    ptr = args[0]
    if ptr:
        emu.mem.set_qw(ptr, retval)
    else:
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'lpPerformanceCount=0x{ptr:X}', res)


### processes & threads


def api_kernel32_dll_ExitProcess(ip, retaddr, args, emu):
    res = 0
    emu.cpu.stop()
    log_call(retaddr, f'', res)


def api_kernel32_dll_GetCurrentProcessId(ip, retaddr, args, emu):
    res = emu.env.fs.get_dw(0x20)
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_GetCurrentProcess(ip, retaddr, args, emu):
    res = 0xFFFFFFFF
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_IsWow64Process(ip, retaddr, args, emu):
    ph, iswow_addr = args
    res = 1
    if iswow_addr:
        emu.mem.set_dw(iswow_addr, 1)
    else:
        res = 0
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'process=0x{ph:X}, ptr=0x{iswow_addr:X}', res)


def api_kernel32_dll_GetCurrentThreadId(ip, retaddr, args, emu):
    res = emu.env.fs.get_dw(0x24)
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_TlsAlloc(ip, retaddr, args, emu):
    res = emu.mem.tls_alloc()
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_TlsGetValue(ip, retaddr, args, emu):
    res = emu.mem.tls_slots[args[0]]
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'{args[0]}', res)


def api_kernel32_dll_TlsSetValue(ip, retaddr, args, emu):
    slot, value = args
    emu.mem.tls_slots[slot] = value
    res = 1
    emu.cpu.api_ret(res, retaddr, 2)
    log_call(retaddr, f'slot={slot}, value=0x{value:08X}', res)


def api_kernel32_dll_FlsAlloc(ip, retaddr, args, emu):
    res = emu.mem.fls_alloc(args[0] or None)
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'callback={args[0]}', res)


def api_kernel32_dll_FlsGetValue(ip, retaddr, args, emu):
    res = emu.mem.fls_get_value(args[0])
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'{args[0]}', res)


def api_kernel32_dll_FlsSetValue(ip, retaddr, args, emu):
    slot, value = args
    emu.mem.fls_set_value(slot, value)
    res = 1
    emu.cpu.api_ret(res, retaddr, 2)
    log_call(retaddr, f'slot={slot}, value=0x{value:08X}', res)


### memory


def api_kernel32_dll_VirtualAlloc(ip, retaddr, args, emu):
    addr, size, alloctype, prot = args
    memva = emu.mem.alloc_mem(size, tag=f'VirtAlloc@{retaddr:X}')
    emu.cpu.api_ret(memva, retaddr, len(args))
    log_call(retaddr, f'0x{size:x} bytes @ 0x{addr:x}, prot=0x{prot:x}', memva)


def api_kernel32_dll_VirtualAllocEx(ip, retaddr, args, emu):
    hproc, addr, size, alloctype, prot = args
    if hproc == 0xFFFFFFFF:
        memva = emu.mem.alloc_mem(size, tag=f'VirtAllocEx@{retaddr:X}')
    else:
        err('Not implemented: cross-process VirtualAllocEx!')
    emu.cpu.api_ret(memva, retaddr, len(args))
    log_call(retaddr, f'proc=0x{hproc:x}, 0x{size:x} bytes @ 0x{addr:x}, prot=0x{prot:x}', memva)


def api_kernel32_dll_LocalAlloc(ip, retaddr, args, emu):
    flags, size = args
    memva = emu.mem.alloc_mem(size, tag=f'LocalAlloc@{retaddr:X}', zero_memory=flags & 0x40)
    emu.cpu.api_ret(memva, retaddr, len(args))
    log_call(retaddr, f'0x{size:x} bytes, flags=0x{flags:x}', memva)


def api_kernel32_dll_GetProcessHeap(ip, retaddr, args, emu):
    res = emu.mem.get_heap(None)['addr']
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_HeapCreate(ip, retaddr, args, emu):
    opt, initsize, size = args
    sz = size or initsize or 4096
    heap = emu.mem.create_heap(initsize or 4096, tag=f'HeapCreate({format_size(sz)})@{retaddr:X}')
    res = heap['addr']
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'opt=0x{opt:X}, initsize=0x{initsize:X}, size=0x{size:X}', res)


def api_kernel32_dll_HeapAlloc(ip, retaddr, args, emu):
    hheap, flags, size = args
    memva = emu.mem.alloc_mem(size, tag=f'HeapAlloc({hheap:X})@{retaddr:X}', heap_id=hheap,
            zero_memory=flags & 0x00000008)
    emu.cpu.api_ret(memva, retaddr, len(args))
    log_call(retaddr, f'heap=0x{hheap:X}, flags=0x{flags:X}, 0x{size:X} bytes', memva)


def api_kernel32_dll_HeapReAlloc(ip, retaddr, args, emu):
    hheap, flags, oldptr, newsize = args
    newptr = 0
    if hheap in emu.mem.heaps:
        oldsize = emu.mem.heaps[hheap]['by_addr'].get(oldptr, None)
        if oldsize:
            newptr = emu.mem.alloc_mem(newsize, tag=f'HeapReAlloc({hheap:X})@{retaddr:X}',
                    heap_id=hheap, zero_memory=flags & 0x00000008)
            emu.mem.write(newptr, emu.mem.read(oldptr, min(newsize, oldsize)))
    emu.cpu.api_ret(newptr, retaddr, len(args))
    log_call(retaddr, f'heap=0x{hheap:X}, flags=0x{flags:X}, oldptr=0x{oldptr:X}', newptr)


def api_kernel32_dll_HeapSize(ip, retaddr, args, emu):
    hheap, flags, ptr = args
    res = emu.mem.heaps[hheap]['by_addr'].get(ptr, (-1,))[0]
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'heap=0x{hheap:X}, flags=0x{flags:X}, ptr=0x{ptr:X}', res)


def api_kernel32_dll_HeapFree(ip, retaddr, args, emu):
    hheap, flags, ptr = args
    memva = emu.mem.free_mem(ptr, heap_id=hheap)
    emu.cpu.api_ret(memva, retaddr, len(args))
    log_call(retaddr, f'heap=0x{hheap:X}, flags=0x{flags:X}, addr=0x{ptr:X}', memva)


def api_kernel32_dll_RtlZeroMemory(ip, retaddr, args, emu):
    addr, length = args
    emu.mem.write(addr, b'\0' * length)
    res = 0
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'0x{addr:X}[0x{length:X}]', res)


# todo: HeapSize(), HeapReAlloc()


### version


def api_kernel32_dll_GetVersion(ip, retaddr, args, emu):
    ver = emu.env.os_ver
    res = ver['major'] | ver['minor'] << 8 | ver['build'] << 16
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_GetVersionExA(ip, retaddr, args, emu):
    ptr = args[0]
    ver = emu.env.os_ver
    if ptr:
        emu.mem.set_dw(ptr + 4, ver['major'])
        emu.mem.set_dw(ptr + 8, ver['minor'])
        emu.mem.set_dw(ptr + 12, ver['build'])
        emu.mem.set_dw(ptr + 16, 2)  # dwPlatformId = NT
        emu.mem.set_dw(ptr + 20, 0)  # szCSDVersion (service pack as string)
        if emu.mem.get_dw(ptr) > 5 * 4 + 128:
            # extended structure: OSVERSIONINFOEXA
            if emu.env.os_sp:
                sp_maj, sp_min = emu.env.os_sp, 0
            else:
                sp_maj = sp_min = 0
            emu.mem.set_dw(ptr + 148, sp_maj)
            emu.mem.set_dw(ptr + 152, sp_min)
            emu.mem.set_dw(ptr + 156, emu.env.os_suite_mask)
            emu.mem.set_dw(ptr + 160, emu.env.os_prod_type)
    else:
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
    res = ver['major'] | ver['minor'] << 8 | ver['build'] << 16
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


### strings


def api_kernel32_dll_WideCharToMultiByte(ip, retaddr, args, emu):
    cp, flags, wstr_addr, wstr_cc, mbstr_addr, mbstr_sz, defchar, used_defchr_addr = args
    wstr = mbstr = None
    if wstr_addr:
        wstr = emu.mem.read(wstr_addr, wstr_cc * 2, encoding='utf16')
        enc = cp_to_encoding(cp)
        if not enc:
            err(f'Unknwon code page: {cp}!')
            enc = 'ascii'
        mbstr = wstr.encode(encoding=enc, errors='replace')
        if mbstr_sz:
            emu.mem.write(mbstr_addr, mbstr[:mbstr_sz])
        res = len(mbstr)
    else:
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
        res = 0
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'cp={cp}, wstr={wstr}, defchar={defchar} => {mbstr}', res)


def api_kernel32_dll_MultiByteToWideChar(ip, retaddr, args, emu):
    cp, flags, mbstr_addr, mbstr_sz, wstr_addr, wstr_cc = args
    wstr = mbstr = None
    if mbstr_addr:
        enc = cp_to_encoding(cp)
        if not enc:
            err(f'Unknwon code page: {cp}!')
            enc = 'ascii'
        mbstr = emu.mem.read(mbstr_addr, mbstr_sz, encoding=enc)
        wstr = mbstr.encode(encoding='utf16', errors='replace')
        if wstr.startswith(b'\xFF\xFE'):
            wstr = wstr[2:]
        if wstr_cc:
            emu.mem.write(wstr_addr, wstr[:wstr_cc * 2])
        res = len(mbstr)    # number of chars!
    else:
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
        res = 0
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'cp={cp}, mbstr={mbstr} => {wstr}', res)


def api_kernel32_dll_GetACP(ip, retaddr, args, emu):
    res = 1252
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_GetCPInfo(ip, retaddr, args, emu):
    cp, addr = args
    buf = struct.pack('<LL', MAX_CP_CHAR_SIZE_REV.get(cp, 1), CP_DEF_CHAR.get(cp, ord('?'))) \
        + 10 * b'\0'
    res = 1
    emu.mem.write(addr, buf)
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'{cp}', res)


def api_kernel32_dll_GetStringTypeW(ip, retaddr, args, emu):
    info_type, wstr_addr, wstr_cc, ctype_addr = args
    wstr = None
    res = 1
    if wstr_addr and ctype_addr:
        wstr = emu.mem.read(wstr_addr, wstr_cc * 2, encoding='utf16')
        if info_type == 1:
            cres = 0
            for i, c in enumerate(wstr):
                if c.isupper():
                    cres |= Win.C1_UPPER
                if c.islower():
                    cres |= Win.C1_LOWER
                if c.isdigit():
                    cres |= Win.C1_DIGIT
                if c.isspace():
                    cres |= Win.C1_SPACE
                if c in '.,;:!?-':     # punctuation
                    cres |= Win.C1_PUNCT
                # fixme: no idea what Win.C1_CNTRL (0x0020) and Win.C1_BLANK (0x0040) mean
                if c.isdigit() or c in 'abcdefABCDEF':
                    cres |= Win.C1_XDIGIT
                if c.isalpha():
                    cres |= Win.C1_ALPHA
                emu.mem.set_w(ctype_addr + i * 2, cres)
        else:
            err(f'Not implemented: GetStringTypeW(dwInfoType={info_type})!')
    else:
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
        res = 0
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{info_type}, {wstr}', res)


def api_kernel32_dll_LCMapStringW(ip, retaddr, args, emu):
    # partially emulated
    locale, flags, src_str, src_cc, dst_str, dst_cc = args
    if src_str:
        dst = emu.mem.read(src_str, src_cc * 2, encoding='utf16')
        if flags & Win.LCMAP_LOWERCASE:
            dst = dst.lower()
        if flags & Win.LCMAP_UPPERCASE:
            dst = dst.upper()
        if flags & Win.LCMAP_TITLECASE:
            dst = dst.title()
    if dst_str and dst_cc:
        emu.mem.write(dst_str, dst.encode('utf16')[:(dst_cc - 1) // 2] + b'\0\0')
    res = len(dst) + 1
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_kernel32_dll_lstrcmpi(ip, retaddr, args, emu):
    s1_addr, s2_addr = args
    s1 = emu.mem.read_pchar(s1_addr).lower()
    s2 = emu.mem.read_pchar(s2_addr).lower()
    res = -1 if s1 < s2 else (0 if s1 == s2 else 1)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{repr(s1)}, {repr(s2)}', res)


def api_kernel32_dll_lstrcpyA(ip, retaddr, args, emu):
    s1_addr, s2_addr = args
    s2 = emu.mem.read_pchar(s2_addr) + b'\0'
    emu.mem.write(s1_addr, s2)
    res = s1_addr
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'0x{s1_addr:X}, {repr(s2)}', res)


def api_kernel32_dll_lstrcatA(ip, retaddr, args, emu):
    s1_addr, s2_addr = args
    s1 = emu.mem.read_pchar(s1_addr)
    s2 = emu.mem.read_pchar(s2_addr) + b'\0'
    emu.mem.write(s1_addr + len(s1), s2)
    res = s1_addr
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'0x{s1_addr:X}->{repr(s1)}, {repr(s2)}', res)


def api_shlwapi_dll_StrStrA(ip, retaddr, args, emu):
    s1_addr, s2_addr = args
    s1 = emu.mem.read_pchar(s1_addr).lower()
    s2 = emu.mem.read_pchar(s2_addr).lower()
    pos = s1.find(s2)
    res = s1_addr + pos if pos > 0 else 0
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{repr(s1)}, {repr(s2)}', res)


def api_shlwapi_dll_StrStrW(ip, retaddr, args, emu):
    s1_addr, s2_addr = args
    s1 = emu.mem.read_pwchar(s1_addr).lower()
    s2 = emu.mem.read_pwchar(s2_addr).lower()
    pos = s1.find(s2)
    res = s1_addr + pos if pos > 0 else 0
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{repr(s1)}, {repr(s2)}', res)


### synchronization


def api_kernel32_dll_InterlockedIncrement(ip, retaddr, args, emu):
    ptr = args[0]
    value = emu.mem.get_dw(ptr) + 1
    emu.mem.set_dw(ptr, value & 0xFFFFFFFF)
    emu.cpu.api_ret(value, retaddr, 1)
    log_call(retaddr, f'0x{ptr:X} (value: 0x{value:X})', value)


def api_kernel32_dll_InterlockedIncrement16(ip, retaddr, args, emu):
    ptr = args[0]
    value = emu.mem.get_w(ptr) + 1
    emu.mem.set_w(ptr, value & 0xFFFF)
    emu.cpu.api_ret(value, retaddr, 1)
    log_call(retaddr, f'0x{ptr:X} (value: 0x{value:X})', value)


def api_kernel32_dll_InterlockedIncrement64(ip, retaddr, args, emu):
    ptr = args[0]
    value = emu.mem.get_qw(ptr) + 1
    emu.mem.set_qw(ptr, value & 0xFFFFFFFFFFFFFFFF)
    emu.cpu.api_ret(value, retaddr, 1)
    log_call(retaddr, f'0x{ptr:X} (value: 0x{value:X})', value)


def api_kernel32_dll_InterlockedDecrement(ip, retaddr, args, emu):
    ptr = args[0]
    value = emu.mem.get_dw(ptr) - 1
    emu.mem.set_dw(ptr, value & 0xFFFFFFFF)
    emu.cpu.api_ret(value, retaddr, 1)
    log_call(retaddr, f'0x{ptr:X} (value: 0x{value:X})', value)


def api_kernel32_dll_InterlockedDecrement16(ip, retaddr, args, emu):
    ptr = args[0]
    value = emu.mem.get_w(ptr) - 1
    emu.mem.set_w(ptr, value & 0xFFFF)
    emu.cpu.api_ret(value, retaddr, 1)
    log_call(retaddr, f'0x{ptr:X} (value: 0x{value:X})', value)


def api_kernel32_dll_InterlockedDecrement64(ip, retaddr, args, emu):
    ptr = args[0]
    value = emu.mem.get_qw(ptr) - 1
    emu.mem.set_qw(ptr, value & 0xFFFFFFFFFFFFFFFF)
    emu.cpu.api_ret(value, retaddr, 1)
    log_call(retaddr, f'0x{ptr:X} (value: 0x{value:X})', value)


def _openmutex(name_addr, encoding, emu):
    if name_addr:
        fun = emu.mem.read_pchar if encoding == 'ascii' else emu.mem.read_pwchar
        name = fun(name_addr, decode=True)
        res = emu.env.mutexes.get(name, 0)
        if not res:
            emu.env.set_last_error(Win.ERROR_FILE_NOT_FOUND)
    else:
        name = None
        res = 0
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
    return name, res


def api_kernel32_dll_OpenMutexA(ip, retaddr, args, emu):
    access, inherit, name_addr = args
    name, res = _openmutex(name_addr, 'ascii', emu)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{name}, access=0x{access:X}, inherit={inherit}', res)


def api_kernel32_dll_OpenMutexW(ip, retaddr, args, emu):
    access, inherit, name_addr = args
    name, res = _openmutex(name_addr, 'utf16', emu)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{name}, access=0x{access:X}, inherit={inherit}', res)


### environment


def api_kernel32_dll_GetCommandLineA(ip, retaddr, args, emu):
    res = emu.env.cmdline_va
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, '', res)


def api_kernel32_dll_GetCommandLineW(ip, retaddr, args, emu):
    res = emu.env.cmdlinew_va
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, '', res)


def api_kernel32_dll_GetEnvironmentStringsA(ip, retaddr, args, emu):
    res = emu.env.env_vars_va
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, '', res)


def api_kernel32_dll_GetEnvironmentStringsW(ip, retaddr, args, emu):
    res = emu.env.env_varsw_va
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, '', res)


def api_kernel32_dll_GetStartupInfoA(ip, retaddr, args, emu):
    addr = args[0]
    res = 0
    if addr:
        emu.mem.write(addr, emu.env.startupinfoa)
    else:
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'0x{addr:X}', res)


def api_kernel32_dll_GetStartupInfoW(ip, retaddr, args, emu):
    addr = args[0]
    res = 0
    if addr:
        emu.mem.write(addr, emu.env.startupinfow)
    else:
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'0x{addr:X}', res)


### files


def api_kernel32_dll_GetStdHandle(ip, retaddr, args, emu):
    n = args[0]
    if n not in (-10, -11, -12):
        emu.env.set_last_error(Win.ERROR_INVALID_PARAMETER)
        res = H_INVALID
    else:
        res = emu.env.handle_by_filename(n)
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'{n}', res)


def api_kernel32_dll_GetFileType(ip, retaddr, args, emu):
    fh = args[0]
    res = emu.env.handles[fh]['type']
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, '0x{fh:H}', res)


### folders


def api_kernel32_dll_GetSystemDirectoryA(ip, retaddr, args, emu):
    buf_addr, size = args
    path = emu.env.paths['system']
    if size < len(path) + 1:
        res = len(path) + 1
        emu.env.set_last_error(Win.ERROR_INSUFFICIENT_BUFFER)
    else:
        res = len(path) + 1
        emu.mem.write(buf_addr, path + '\0', encoding='utf8')
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'buffer @0x{buf_addr:X}[{size}]', res)


def api_kernel32_dll_GetSystemDirectoryW(ip, retaddr, args, emu):
    buf_addr, size = args
    path = emu.env.paths['system']
    if size < len(path) + 1:
        res = len(path) + 1
        emu.env.set_last_error(Win.ERROR_INSUFFICIENT_BUFFER)
    else:
        res = len(path)
        emu.mem.write(buf_addr, path + '\0', encoding='utf16')
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'buffer @0x{buf_addr:X}[{size}]', res)


### (raw) socket


def api_wsock32_dll_socket(ip, retaddr, args, emu):
    af, stype, proto = args
    emu.env.set_wsa_last_error(0)
    res = emu.env.new_socket(af, stype, proto)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'af={Net.AF[af]}, type={Net.SOCK[stype]}, proto={Net.IPPROTO[proto]}', res)


def api_wsock32_dll_connect(ip, retaddr, args, emu):
    s, sa_addr, sa_size = args
    emu.env.set_wsa_last_error(0)
    sobj = emu.env.sockets.get(s, None)
    host, port = None, None
    if sobj and sa_addr:
        res = 0
        sa = Net.win_sockaddr(emu.mem.read(sa_addr, sa_size))
        host, port = sa['host'], sa['port']
    else:
        res = Net.SOCKET_ERROR
        emu.env.set_wsa_last_error(Net.WSAEINVAL)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f's=0x{s:X}, host={host}, port={port}', res)


def api_wsock32_dll_send(ip, retaddr, args, emu):
    s, buf_addr, buf_size, flags = args
    emu.env.set_wsa_last_error(0)
    buf = None
    if buf_addr:
        buf = emu.mem.read(buf_addr, buf_size)
        res = len(buf)
    else:
        emu.env.set_wsa_last_error(Net.WSAEINVAL)
        res = 0
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f's=0x{s:X}, buf={buf}', res)


def api_wsock32_dll_recv(ip, retaddr, args, emu):
    global RECV_CNT
    RECV_CNT += 1
    if RECV_CNT >= RECV_MAX_CNT:
        err(f'Hit maximum number of emulated recv()s ({RECV_MAX_CNT}), stopping.')
        emu.cpu.stop()
        return
    s, buf_addr, buf_size, flags = args
    emu.env.set_wsa_last_error(Net.WSAETIMEDOUT)
    res = 0
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f's=0x{s:X}, buf@0x{buf_addr:X}[{buf_size}]', res)


def api_wsock32_dll_WSAStartup(ip, retaddr, args, emu):
    res = 0
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'reqver=0x{args[0]:X}, wsadata=0x{args[1]:X}', res)


def api_wsock32_dll_WSAGetLastError(ip, retaddr, args, emu):
    res = emu.env.wsa_last_error
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'', res)


def api_wsock32_dll_htons(ip, retaddr, args, emu):
    val = args[0]
    res = (val & 0xFF) << 8 | (val >> 8)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'value=0x{val:04X}', res)


def api_wsock32_dll_htonl(ip, retaddr, args, emu):
    val = args[0]
    res = int.from_bytes(val.to_bytes(4, byteorder='little'), byteorder='big', signed=False)
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'value=0x{val:08X}', res)


def api_wsock32_dll_inet_addr(ip, retaddr, args, emu):
    cp_addr = args[0]
    cp = emu.mem.read_pchar(cp_addr, decode=True)
    res = int.from_bytes(socket.inet_aton(cp), byteorder='little')
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'{cp}', res)


### misc


def api_kernel32_dll_EncodePointer(ip, retaddr, args, emu):
    ptr = args[0]
    res = ptr ^ 0xA5A5A5A5
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'0x{ptr:X}', res)


def api_kernel32_dll_DecodePointer(ip, retaddr, args, emu):
    ptr = args[0]
    res = ptr ^ 0xA5A5A5A5
    emu.cpu.api_ret(res, retaddr, 1)
    log_call(retaddr, f'0x{ptr:X}', res)


### msvcrt internals


def api_msvcrt_dll___set_app_type(ip, retaddr, args, emu):
    at = emu.mem.read_stack(1, 4)[0]
    res = 0
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'apptype={at}', res)


def api_msvcrt_dll___p__fmode(ip, retaddr, args, emu):
    res = emu.env.fmode_addr
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_msvcrt_dll___p__commode(ip, retaddr, args, emu):
    res = emu.env.commode_addr
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'', res)


def api_msvcrt_dll__controlfp(ip, retaddr, args, emu):
    new, mask = emu.mem.read_stack(2, 4)
    res = 0x010D021B
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'new=0x{new:X}, mask=0x{mask:X}', res)


def api_msvcrt_dll__initterm(ip, retaddr, args, emu):
    p1, p2 = emu.mem.read_stack(2, 4)
    res = 0
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'ptr1=0x{p1:X}, ptr2=0x{p2:X}', res)


def api_msvcrt_dll___getmainargs(ip, retaddr, args, emu):
    argc_addr, argv_addr, env_addr, wildcard_addr, startupinfo_addr = emu.mem.read_stack(5, 4)
    res = 0
    emu.mem.set_dw(argc_addr, len(emu.env.run_args))
    emu.mem.set_dw(argv_addr, emu.env.argv_va)
    emu.mem.set_dw(env_addr, emu.env.env_vars_va)
    emu.cpu.api_ret(res, retaddr, 0)
    log_call(retaddr, f'argc@0x{argc_addr:X}, argv@0x{argv_addr:X}, env@0x{env_addr:X}, '
            f'startupinfo@0x{startupinfo_addr:X}', res)


def api_msvcrt_dll_exit(ip, retaddr, args, emu):
    status = emu.mem.read_stack(1, 4)[0]
    log_call(retaddr, f'status={status}', 0)
    emu.cpu.stop()


### Windows registry


def api_advapi32_dll_RegOpenKeyA(ip, retaddr, args, emu):
    hkey, subkey_addr, regkey_addr = args
    subkey = None
    if subkey_addr:
        subkey = emu.mem.read_pchar(subkey_addr, decode=True)
    handle = emu.env.winreg_open(subkey, parent=hkey, create=False)
    if handle:
        res = 0
        emu.mem.set_dw(regkey_addr, handle)
    else:
        res = Win.ERROR_FILE_NOT_FOUND
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'parent={Win.NAME_HKEY[hkey]}, subkey={subkey}, regkey@0x{regkey_addr:X}',
            res)


def api_advapi32_dll_RegQueryValueExA(ip, retaddr, args, emu):
    hkey, name_addr, _, type_addr, data_addr, data_size_addr = args
    res, data, path, data_size = Win.ERROR_FILE_NOT_FOUND, None, None, None
    if hkey in emu.env.regkeys and type_addr and data_size_addr:
        regkey = emu.env.regkeys[hkey]
        path = regkey['path']
        name = "" if not name_addr else emu.mem.read_pchar(name_addr, decode=True)
        if name in regkey['data']:
            data = regkey['data'][name]
            vtype = emu.mem.get_dw(type_addr)
            if type(data) is int:
                fmt = 'L' if vtype in (Win.REG_DWORD, Win.REG_DWORD_BIG_ENDIAN) else 'Q'
                end = '<' if vtype in (Win.REG_DWORD, Win.REG_QWORD) else '>'
                data = struct.pack(end + fmt, data)
            else:
                data = data.encode('utf8', errors='replace')
            data_size = emu.mem.get_dw(data_size_addr)
            res = 0
            if data_size < len(data):
                res = Win.ERROR_MORE_DATA
            emu.mem.set_dw(data_size_addr, len(data))
            if data_addr:
                emu.mem.write(data_addr, data[:data_size])
    emu.cpu.api_ret(res, retaddr, len(args))
    log_call(retaddr, f'key={path}, name="{name}", type={Win.REG[vtype]}, buf_size={data_size} => {data}',
        res)

