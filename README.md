# mwemu

Malware analysis emulator written in Python 3 based on
[Unicorn](https://github.com/unicorn-engine/unicorn). Also uses
[pefile](https://github.com/erocarrera/pefile) and [Capstone](https://github.com/aquynh/capstone).

Vlad Topan (vtopan / gmail), Apr. 2019

![Sample log](docs/sample-log.png)


## Why

The initial goal was a quick script to unpack specific malware families and trace API calls. One
thing led to another and a generic malware emulator was born. 32-bit and 64-bit PE/PE+ files are
handled as of this writing, but it's designed with other executable formats (e.g. ELF) in mind (and
once ELF's done, ARM support should be easy enough to add).

If time permits, it will become capable to unpack malware samples (with custom scripting/recipes),
trace API calls and possibly cook breakfast.


## How it works

The code emulation is done using the *Unicorn* emulator. The main component (`mwemu.py`) is both a
"global object" and the "sample" tool exposing the emulator's behavior. The components are as
follows:


### CPU

The CPU emulator component (`libemucpu.py`) handles the Unicorn emulator internals, hooks, register
operations etc.


### Environment

An *environment* (`libemuenv.py`) is set up:

- GDT / MSR entries to allow `FS:*` handling
- TEB, PEB, PEB_LDR, STARTUP_INFO structures
- basic CRT emulation (`fmode`, `commode`)
- TLS callbacks, data access APIs
- stack
- heap (initialized on demand at the first allocation)
- standard I/O file handles
- environment strings
- Windows Registry (backed by a JSON, `data/win-registry.json`, describing the contents)
- sockets (partial WSA emulation)
- threads
- etc.


### Loader

A *loader* component (`libldr.py`) parses the executable (in particular its imports), and using a
"mockup" DLL template (`template-dll/template32.asm`) it generates imported DLLs on demand with the
needed exports (based on the sample's IAT). All "mock" APIs clear the arguments off the stack, set
`LastError` to 0 and return 1, and some APIs are explicitly emulated in `libwinapi.py` (more to
come). The API arguments are automagically inferred from DLL exports and Windows SDK headers (see
`tools/genwinapiinfo.py` and `tools/dll-list.txt`). Exports are also added dynamically on
`GetProcAddress()`.

The automatically-extracted API prototypes are in `data/X86/*.dll.json`; manually added symbols
have an additional `.override` extension.


### Memory

The *memory manager* component (`libmemmgr.py`) handles emulator memory and allocations on the
virtual heap(s).


### Others

The disassembler (*Capstone*) is lightly wrapped in `libdisasm.py`, the disassembly is annotated
with known symbols if any. A sliceable `bytes`-like view of the emulator memory is implemented in
`libbinview.py`, as well as a generic one. The output is handled by `libmsg.py`. Misc functions are
in `libmisc.py`.


## Status

This is a late Alpha release - I have used it to unpack some tiny downloaders (*TinyPOS*) with long
decryption stages (takes ~20-30s apiece, faster than spinning up a VM and doing it manually) and a
few other malware samples.

Only some APIs are emulated (all others which are "known" just clear the stack and return TRUE).
Much is left to be done (see below). Some parts are marked as `# todo:` / `# fixme:` through the
code, some are hacked in and need serious reworking, and some are missing altogether - expect bugs /
crashes.


## Installation

Git clone (`git clone https://gitlab.com/vtopan/mwemu`) and install the afore-mentioned Python
modules (run `pip3 install -r requirements.txt` in the project folder).


## Usage

To give it a try, run `mwemu.py <filename>` (with reasonably-set expectations). A flood of
information should fill the screen (it all goes to `stderr`, so to collect it append a `2> log` to
the command). To get less information, use `-b` (brief). To see each instruction as it's emulated,
append a `-s` (step). At the end of the execution, the main image can be dumped (`-dm`) to
`{original-filename}.dump` and each allocated heap buffer to `heap-*.dump` (`-dha`); full heap dumps
are `-dh`. To dump a "mocked" library for debugging use `-dl` (e.g. `-dl kernel32.dll`). For other
command-line arguments use `-h`.

Alternatively, (from a Python source) import `mwemu` and use a `MwEmu()` instance to do custom stuff
(handle specific events, dump specific information / memory, etc.).


## Todo

- initialize stack properly
    - pointers into ntdll.dll?
- create SEH entries
- fill in more PEB / TEB fields
- proper unpack API - fix imports
- emulate more environment variables
- emulate file operations (search, create, etc.)
    - small filesystem as zipfile
- non-main thread handling
- ELF support (including basic POSIX)
- emulate more functions:
    - remote threads
    - synchronization?
    - ntdll.*
    - etc.


## Feedback

Feedback is most welcome at *vtopan / gmail* or as issues on
[gitlab](https://gitlab.com/vtopan/mwemu).

